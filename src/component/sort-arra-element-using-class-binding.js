import React, { useState } from 'react'

export default function ClassBinding() {
    const [city] = useState(['Delhi', 'Utter Pradesh', 'Madhya Pradesh', 'Chattishgarh', 'Kerla', "Maharastra"]);
    const [iconClass, setIconClass] = useState('bi bi-sort-alpha-down');
    function changeClass(e) {
        if (e.target.className == 'bi bi-sort-alpha-down') {
            // up arrow sign
            setIconClass('bi bi-sort-alpha-up');
            //sort array
            city.sort();
        } else {
            // down arrow sign
            setIconClass('bi bi-sort-alpha-down');
            //sort array
            city.sort();
            //reverse array
            city.reverse();
        }
    }
    return (
        <div className='container-fluid'>
            <h2>City <i className={iconClass} onClick={changeClass}/></h2>
            <ol>
                {
                    city.map((item) =>
                        <li>{item}</li>
                    )
                }
            </ol>
        </div>
    )
}
