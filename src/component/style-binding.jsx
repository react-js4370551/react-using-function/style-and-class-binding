import React, { useState } from 'react'

export default function StyleBinding() {
  const [name, setName] = useState("");
  const [styleObject, setStyleObject] = useState({
    border : '',
    boxShadow : ''
  })
  function changeStyleOfInputBox(e){
    setName(e.target.value);
    if(name == ""){
        setStyleObject({
            border : '2px solid red',
            boxShadow : '2px 2px 2px red'
        });
    }else{
        setStyleObject({
            border : '2px solid green',
            boxShadow : '2px 2px 2px green'
        });
    }

  }
  return (
    <div>
        <input type='text' onBlur={changeStyleOfInputBox} style={styleObject}/>
    </div>
  )
}
