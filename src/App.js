import './App.css';
import ClassBinding from './component/sort-arra-element-using-class-binding';
import StyleBinding from './component/style-binding';

function App() {
  return (
    <div>
      {/* <StyleBinding /> */}
      <ClassBinding />
    </div>
  );
}

export default App;
